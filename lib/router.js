//================================================================================
//  @file         router.js
//  @version      0.0.1
//  @path         lib/
//  @description  Parâmetros do pluguin iron:router que define as rotas do sistema.
//  @author       Winny
//  @contact      http://swellitsolutions.com.br/contato
//  @copyright    Copyright Swell It Solutions.
//================================================================================

Router.configure({
	layoutTemplate: 'layoutDefault'
});

Router.map(function(){

	/*Login*/
	Router.route('/', function () {
		this.layout('layoutLogin');
		this.render('signin');
	});

	Router.route('/forgotpassword', function () {
		this.layout('layoutLogin');
		this.render('forgotpassword');
	});

	/*System*/
	Router.route('/notifications', function () {
		this.layout('layoutDefault');
		this.render('notifications');
	});

	Router.route('/management', function () {
		this.layout('layoutDefault');
		this.render('management');
	});

	Router.route('/config', function () {
		this.layout('layoutDefault');
		this.render('config');
	});
});
	