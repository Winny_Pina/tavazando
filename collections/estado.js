//================================================================================
//  @file         estado.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar os Estados Brasileiros.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

 Estado = new Meteor.Collection("Tab_Estado");