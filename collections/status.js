//================================================================================
//  @file         status.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar o Status.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

 Status = new Meteor.Collection("Tab_Status");