//================================================================================
//  @file         tipo_ocorrencia.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar os Estados Brasileiros.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

TpOcorrencia = new Meteor.Collection("Tp_Ocorrencia");
if (Meteor.isServer) {
  if (TpOcorrencia.find().count() === 0) {
    TpOcorrencia.insert({
    	"_id" : "55d4f98c1c3ce30e2aa4257c",
    	"Tipo" : 1,
    	"Descricao" : "Vazamento de água"
    });
    TpOcorrencia.insert({
    	"_id" : "s5d4f98c1c3ce30e2aa4257c",
    	"Tipo" : 4,
    	"Descricao" : "Vazamento de bananas"
    });
  }
}
