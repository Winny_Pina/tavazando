//================================================================================
//  @file         ocorrencias.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar as ocorrencias enviadas do app via WS para o BD.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

 Ocorrencias = new Meteor.Collection("Tab_Ocorrencia");
/*
 * Status:
 * - 0 : Aberto
 * - 1 : Em Andamento
 * - 2 : Concluido
*/

// Fixtures:
if (Meteor.isServer) {
  if (Ocorrencias.find().count() === 0) {
    console.log("Inserindo fixtures de Ocorrencias");
    Ocorrencias.insert({
    	"_id" : "55de137a3fd1c8710147f5fb",
    	"tipo" : 1,
    	"descricao" : "Vazamento de asfalto no meio da agua.",
    	"endereco" : "Av. Missler 436",
    	"cep" : "89140-000",
    	"cidadeId" : "4206900",
    	"local" : [
    		-27.0136748,
    		-49.5288116
    	],
    	"data" : new Date(),
    	"__v" : 0,
      "arquivo" : "http://assets.lwsite.com.br/uploads/widget_image/image/360/36054/agua.jpg",
      "status": 0
    });
    Ocorrencias.insert({
    	"_id" : "55de12dd3fd1c8710147f5fa",
    	"tipo" : 4,
    	"descricao" : "Vazamento de agua no meio do asfalto.",
    	"endereco" : "Rua Principal XYZ",
    	"cep" : "89.140-001",
    	"cidadeId" : "São Paulo",
    	"local" : [
    		-49.4879879,
    		-48.5487958
    	],
    	"data" : new Date(),
    	"__v" : 0,
      "status": 0
    });
  }
}
