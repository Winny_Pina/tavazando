//================================================================================
//  @file         empresas.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar as Empresas de Saneamento Brasileiras.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

 Empresas = new Meteor.Collection("Tb_Emp_Tratamento");