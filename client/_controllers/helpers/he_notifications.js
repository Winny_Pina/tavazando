//================================================================================
//  @file         he_notifications.js
//  @version      0.0.1
//  @path         client/_controllers/helpers
//  @description  HELPERS para Ocorrencias.
//  @author       Winny
//  @contact      http://swellitsolutions.com.br/contato
//  @copyright    Copyright Swell It Solutions.
//================================================================================
if (Meteor.isClient) {
	Template.notifications.helpers({
		status_equals: function(ocorrencia_status, this_status) {
			return ocorrencia_status === this_status;
		},
 		ocorrencias: function() {
 			return Ocorrencias.find().fetch();
		},
		descricao_helper: function(this_tipo) {
			if (this_tipo) {
 				Meteor.subscribe("single_tipoocorrenciaspublish", this_tipo);
				var tipo_ocorrencia = TpOcorrencia.findOne({Tipo: this_tipo});
 				return  tipo_ocorrencia && tipo_ocorrencia.Descricao;
 			}
 		},
		current_ocorrencia: function() {
			var this_id = Session.get("current_ocorrencia_id");
			if (this_id) {
				return Ocorrencias.findOne({_id: this_id});
			}
		}
 	});
 }
