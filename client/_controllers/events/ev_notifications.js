Template.notifications.events({
  'click .modal-trigger': function(event){
    var this_id = $(event.target).parent().attr("data-id");
    if (this_id) {
      Session.set("current_ocorrencia_id", this_id);
    } else {
      console.log("ERROR: COULD NOT GET THE DAMN ID");
    }
  },
  'change [name=change_status_select]': function(event) {
    var this_new_status = parseInt($(event.target).val());
    var this_ocorrencia = $(event.target).attr("data-id");
    Meteor.call("change_ocorrencia_status", this_ocorrencia, this_new_status, function(error, result) {
      if (!error && result) {
        console.log("status atualizado com sucesso");
      } else {
        console.log("erro ao atualizar status");
      }
    });
  }
});
