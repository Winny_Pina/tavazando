//================================================================================
//  @file         _signatureSubscribe.js
//  @version      0.0.1
//  @path         client/_controllers
//  @description  Assinatura para os methods de CRUD no Server.
//  @author       Winny
//  @contact      http://swellitsolutions.com.br/contato
//  @copyright    Copyright Swell It Solutions.
//================================================================================

if (Meteor.isClient) {

  // Subscribe - Tab_Ocorrencia
  // -------------------
  Meteor.subscribe('ocorrenciaspublish');
}
