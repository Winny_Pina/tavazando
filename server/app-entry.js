//================================================================================
//  @file        	app-entry.js
//  @version     	0.0.1
//  @path        	server/
//  @description  Ao iniciar a ãplicação, o servidor roda este arquivo para
//							 	monitorar e iniciar as tabelas não iniciadas.
//  @author      	Winny
//  @contact     	http://swellitsolutions.com.br/contato
//  @copyright   	Copyright Swell It Solutions.
//================================================================================

if (Meteor.isServer) {
	Meteor.startup(function () {
		console.log('##################################### Server Sartup #####################################');
	});
}