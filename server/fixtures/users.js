if (Meteor.users.find().count() === 0) {
  console.log("Inserting root user");
  // Login: vivianeppina@hotmail.com
  // Password: 123456

  Meteor.users.insert({
  	"_id" : "root",
  	"createdAt" : new Date(),
  	"services" : {
  		"password" : {
  			"bcrypt" : "$2a$10$F13BqU4yEtBYFw.6pMtpH.623zJbP1M.Bvzupi6G59wOlzXZRCI7u"
  		},
  		"resume" : {
  			"loginTokens" : []
  		}
  	},
  	"emails" : [
  		{
  			"address" : "vivianeppina@hotmail.com",
  			"verified" : true
  		}
  	],
  	"profile" : {
  		"name" : "root",
      "root": true,
  		"ceps" : [
  		]
  	}
  }
  );

  Meteor.users.insert({
  	"_id" : "casan",
  	"createdAt" : new Date(),
  	"services" : {
  		"password" : {
  			"bcrypt" : "$2a$10$F13BqU4yEtBYFw.6pMtpH.623zJbP1M.Bvzupi6G59wOlzXZRCI7u"
  		},
  		"resume" : {
  			"loginTokens" : []
  		}
  	},
  	"emails" : [
  		{
  			"address" : "admin@casan.com.br",
  			"verified" : true
  		}
  	],
  	"profile" : {
  		"name" : "root",
      "root": false,
  		"ceps" : [
  			"89160-000"
  		]
  	}
  }
  );

  Meteor.users.insert({
  	"_id" : "sabesp",
  	"createdAt" : new Date(),
  	"services" : {
  		"password" : {
  			"bcrypt" : "$2a$10$F13BqU4yEtBYFw.6pMtpH.623zJbP1M.Bvzupi6G59wOlzXZRCI7u"
  		},
  		"resume" : {
  			"loginTokens" : []
  		}
  	},
  	"emails" : [
  		{
  			"address" : "admin@sabesp.com",
  			"verified" : true
  		}
  	],
  	"profile" : {
  		"name" : "root",
      "root": false,
  		"ceps" : [
  			"89160-000"
  		]
  	}
  }
  );
}
