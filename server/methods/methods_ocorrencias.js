Meteor.methods({
  change_ocorrencia_status: function(ocorrencia_id, new_status) {
    check(ocorrencia_id, String);
    check(new_status, Number);
    // the check cannot  be made with > < because it shouuld be an integer
    if (new_status !== 0 && new_status !== 1 && new_status !== 2) {
      throw new Error("Invalid status");
    }
    // Double negation to force the return of a boolean
    return !!Ocorrencias.update({_id: ocorrencia_id}, {"$set": {status: new_status}});
  }
});
