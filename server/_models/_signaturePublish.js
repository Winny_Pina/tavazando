//================================================================================
//  @file         _signaturePublish.js
//  @version      0.0.1
//  @path         server/
//  @description    Cria as assinaturas publicas para os methods de CRUD.
//  @author      	Winny
//  @contact     	http://swellitsolutions.com.br/contato
//  @copyright   	Copyright Swell It Solutions.
//================================================================================

if (Meteor.isServer) {
  // Publish - ocorrenciaspublish
  // -----------------
  Meteor.publish('ocorrenciaspublish', function() {
    return Ocorrencias.find({});
  });
}
if (Meteor.isServer) {
  // Publish - single_tipoocorrenciaspublish
  // -----------------
  Meteor.publish('single_tipoocorrenciaspublish', function (_tipo) {
    return TpOcorrencia.find({Tipo: _tipo});
  });
}
